package com.iterge.iterge_pre.redis;

import com.iterge.iterge_pre.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author liuph
 * @date 2023/9/26 14:36:45
 */
@SpringBootTest
public class RedisTest {
    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void stringTest(){
        User user = new User();
        user.setName("zhang san");
        user.setId(1);
        redisTemplate.opsForValue().set("user",user.toString());
    }

    @Test
    public void getStringTest(){
        Object name = redisTemplate.opsForValue().get("user");
        System.out.println(name);
    }
}
