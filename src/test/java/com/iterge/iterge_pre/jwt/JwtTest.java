package com.iterge.iterge_pre.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.jupiter.api.Test;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author liuph
 * @date 2023/10/13 17:45:59
 */
@SpringBootTest
public class JwtTest {
    private static String SECRET = "iterge";

    @Test
    public void createJwt(){
        // 载核【Payload】
        Map<String, Object> payload = new HashMap<>();
        payload.put("sub", "1234567890");
        payload.put("name","John Doe");
        payload.put("admin",true);

        // 声明Token失效时间
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.SECOND,300);

        String token = Jwts.builder()
                .setIssuer("lph")
                .setIssuedAt(new Date())//设置签发时间
                .setClaims(payload)//设置载核
                .setExpiration(instance.getTime())//设置生效时间
                .signWith(SignatureAlgorithm.HS256,SECRET)//通过HS256加密方式生成签名,这里采用私钥进行签名
                .compact();
        System.out.println(token);
    }

    @Test
    public void parseJwt(){
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImV4cCI6MTY5NzE5MTE4Nn0.NBy1NH1yNM40qIebXGhTvRd6MPOg5kCPLrYZwyFEw6w";
        Claims lp = Jwts.parser().setSigningKey(SECRET)
                .parseClaimsJws(token)
                .getBody();

        System.out.println(lp);
    }
}
