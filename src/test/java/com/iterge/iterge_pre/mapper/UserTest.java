package com.iterge.iterge_pre.mapper;

import com.iterge.iterge_pre.entity.User;
import com.iterge.iterge_pre.entity.UserInfo;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author liuph
 * @date 2023/9/1 16:02:35
 */
@SpringBootTest
public class UserTest {
    @Resource
    private UserMapper userMapper;
    @Resource
    private UserInfoMapper userInfoMapper;

    @Test
    public void test(){
        User user = userMapper.selectById(1);
        System.out.println(user.toString());
    }


    @Test
    public void test1(){
        UserInfo userInfo = userInfoMapper.selectById(1);
        System.out.println(userInfo.toString());
    }
}
