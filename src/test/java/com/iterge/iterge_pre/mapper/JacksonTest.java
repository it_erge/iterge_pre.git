package com.iterge.iterge_pre.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iterge.iterge_pre.entity.UInfo;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author liuph
 * @date 2023/9/22 11:09:15
 */
@SpringBootTest
@Slf4j
public class JacksonTest {

    ObjectMapper mapper = new ObjectMapper();

    @SneakyThrows
    @Test
    public void test1(){
        UInfo uInfo = new UInfo();
        uInfo.setFirstName("san");
        uInfo.setLastName("zhang");
        uInfo.setAge(20);
        System.out.println(mapper.writeValueAsString(uInfo));
    }

    @SneakyThrows
    @Test
    public void test2(){
        String str = "{\"firstName\":\"san\",\"lastName\":\"zhang\",\"age\":20}";
        UInfo uInfo = mapper.readValue(str, UInfo.class);
        log.info("first name:{}",uInfo.getFirstName());
    }
}
