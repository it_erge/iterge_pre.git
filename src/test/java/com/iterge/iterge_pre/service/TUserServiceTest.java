package com.iterge.iterge_pre.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.iterge.iterge_pre.entity.TUser;
import com.iterge.iterge_pre.entity.User;
import com.iterge.iterge_pre.mapper.TUserMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author liuph
 * @date 2023/9/6 10:49:02
 */

@SpringBootTest
@Slf4j
public class TUserServiceTest {
    @Resource
    private ITUserService itUserService;

    @Test
    public void selectById(){
        Optional<TUser> optById = itUserService.getOptById(1);
        log.info("通过id查询数据：{}",optById.get());
    }

    @Test
    public void queryAll(){
        List<TUser> list = itUserService.list();
        log.info("获取全量数据：{}",list.size());
    }

    @Test
    public void queryByUser(){
        LambdaQueryWrapper<TUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(TUser::getName,"zhangsan");
        List<TUser> list = itUserService.list(queryWrapper);
        log.info("自定义条件查询：{}",list.size());
    }

    @Test
    public void queryByPage(){
        IPage<TUser> page = new Page<>();
        page.setSize(2)
            .setCurrent(2);
        LambdaQueryWrapper<TUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(TUser::getName,"test");
        List<TUser> list = itUserService.list(page,queryWrapper);
        log.info("分页查询：{}",list.size());
    }
}
