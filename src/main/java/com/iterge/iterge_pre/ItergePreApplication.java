package com.iterge.iterge_pre;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
@MapperScan("com.iterge.iterge_pre.mapper")
public class ItergePreApplication {

    public static void main(String[] args) {
        SpringApplication.run(ItergePreApplication.class, args);
        log.info("启动成功～");
    }

}
