package com.iterge.iterge_pre.service.impl;

import cn.dev33.satoken.stp.StpInterface;
import cn.hutool.core.convert.Convert;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liuph
 * @date 2023/10/12 14:14:39
 */
@Slf4j
@Component
public class StpInterfaceImpl implements StpInterface {

    /**
     * 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        // 本 list 仅做模拟，实际项目中要根据loginId获取具体业务逻辑来查询权限
        //因为每次的接口权限验证，都要先执行该方法获取登录用户的权限码，所以建议生产环境中将权限码和角色类型信息放到redis中
        List<String> list = new ArrayList<>();
        if(Convert.toInt(loginId) == 1){
            list.add("101");
            list.add("user.add");
            list.add("user.update");
            list.add("user.get");
            list.add("art.*");
        }
        if(Convert.toInt(loginId) == 2){
            list.add("user.update");
            list.add("user.get");
            list.add("user.delete");
            list.add("art.*");
        }

        return list;
    }

    /**
     * 返回一个账号所拥有的角色标识集合 (权限与角色可分开校验)
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        // 本 list 仅做模拟，实际项目中要根据具体业务逻辑来查询角色
        List<String> list = new ArrayList<>();
        if(Convert.toInt(loginId) == 1){
            list.add("super-admin");
        }
        list.add("admin");
        return list;
    }
}
