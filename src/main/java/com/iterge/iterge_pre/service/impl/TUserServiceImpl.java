package com.iterge.iterge_pre.service.impl;

import com.iterge.iterge_pre.entity.TUser;
import com.iterge.iterge_pre.mapper.TUserMapper;
import com.iterge.iterge_pre.service.ITUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iterge
 * @since 2023-09-05
 */
@Service
public class TUserServiceImpl extends ServiceImpl<TUserMapper, TUser> implements ITUserService {

}
