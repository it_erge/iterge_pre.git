package com.iterge.iterge_pre.service.impl;

import com.iterge.iterge_pre.entity.UserInfo;
import com.iterge.iterge_pre.mapper.UserInfoMapper;
import com.iterge.iterge_pre.service.IUserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iterge
 * @since 2023-09-05
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements IUserInfoService {

}
