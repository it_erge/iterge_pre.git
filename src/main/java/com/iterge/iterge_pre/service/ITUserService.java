package com.iterge.iterge_pre.service;

import com.iterge.iterge_pre.entity.TUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iterge
 * @since 2023-09-05
 */
public interface ITUserService extends IService<TUser> {

}
