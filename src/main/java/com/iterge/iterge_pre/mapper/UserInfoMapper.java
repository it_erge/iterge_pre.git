package com.iterge.iterge_pre.mapper;

import com.iterge.iterge_pre.entity.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author iterge
 * @since 2023-09-05
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}
