package com.iterge.iterge_pre.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iterge.iterge_pre.entity.User;

/**
 * @author liuph
 * @date 2023/9/1 16:01:44
 */
public interface UserMapper extends BaseMapper<User> {
}
