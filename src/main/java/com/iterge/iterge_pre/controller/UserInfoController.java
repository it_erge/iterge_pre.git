package com.iterge.iterge_pre.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author iterge
 * @since 2023-09-05
 */
@RestController
@RequestMapping("/userInfo")
public class UserInfoController {

}
