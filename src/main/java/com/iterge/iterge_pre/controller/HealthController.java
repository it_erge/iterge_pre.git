package com.iterge.iterge_pre.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuph
 * @date 2023/8/31 16:11:28
 */
@RestController
public class HealthController {
    @GetMapping("/hello")
    public String hello(){
        return "hello world!";
    }
}
