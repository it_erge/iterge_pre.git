package com.iterge.iterge_pre.controller;

import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import com.iterge.iterge_pre.entity.Response;
import com.iterge.iterge_pre.entity.UInfo;
import com.iterge.iterge_pre.exception.BizException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author iterge
 * @since 2023-09-05
 */
@RestController
@RequestMapping("/tUser")
@Slf4j
public class TUserController {
    @GetMapping("toLogin/{id}")
    public Response<String> toLogin(@PathVariable("id") Integer id){
        StpUtil.login(id);
        log.info("成功");
        return Response.ok("登录成功");
    }

    @GetMapping("test")
    public Response<UInfo> get(){
        //检查是否登录
        StpUtil.checkLogin();
        UInfo uInfo = new UInfo();
        uInfo.setFirstName("san ge");
        uInfo.setLastName("zhang");
        uInfo.setAge(201);
        return Response.ok(uInfo);
    }

    @GetMapping("test/{id}")
    public Response<String> getUser(@PathVariable("id") Integer id){
        if(!StpUtil.hasPermission("101")){
            log.info("权限101验证未成功");
            throw new BizException(202,"账号没有该操作权限!");
        }
        return Response.ok("权限通过成功");
    }

    @GetMapping("check/{id}")
    public Response<String> check(@PathVariable("id") Integer id){
        if(!StpUtil.hasPermission("user.update")){
            log.info("权限user.update验证未成功");
            throw new BizException(202,"账号没有该操作权限!");
        }
        if(!StpUtil.hasRole("super-admin")){
            throw new BizException(203,"账号没有相应角色!");
        }
        return Response.ok("权限通过成功");
    }

    // 查询 Token 信息
    @RequestMapping("tokenInfo")
    public Response<SaTokenInfo> tokenInfo() {
        return Response.ok(StpUtil.getTokenInfo());
    }
}
