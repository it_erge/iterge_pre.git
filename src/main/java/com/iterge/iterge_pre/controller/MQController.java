package com.iterge.iterge_pre.controller;

import com.iterge.iterge_pre.entity.Response;
import com.iterge.iterge_pre.mq.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuph
 * @date 2023/10/17 15:58:32
 */
@RestController
@RequestMapping("mq")
public class MQController {
    @Autowired
    private ProducerService producerService;

    @GetMapping("send/{msg}")
    public Response<String> send(@PathVariable(value = "msg") String mag){
        producerService.sendMag(mag);
        return Response.ok();
    }
}
