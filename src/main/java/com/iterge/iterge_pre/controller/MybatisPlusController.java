package com.iterge.iterge_pre.controller;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.iterge.iterge_pre.entity.User;
import com.iterge.iterge_pre.entity.UserInfo;
import com.iterge.iterge_pre.mapper.UserMapper;
import com.iterge.iterge_pre.service.impl.UserInfoServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author liuph
 * @date 2023/9/4 14:52:46
 */
@RestController
@Slf4j
public class MybatisPlusController {
    @Resource
    private UserInfoServiceImpl userInfoService;

    @GetMapping("/test/get")
    public UserInfo getUser(Integer id){
        log.info("请求入参：{}",id);
        UserInfo user = userInfoService.getById(id);
        log.info("请求结果：{}",user.getFirstName());
        log.info("你看，这样很明显知道我们是一次请求的log吧！");
        return user;
    }
}
