package com.iterge.iterge_pre.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author liuph
 * @date 2023/9/1 16:00:50
 */
@Data
@TableName("t_user")
public class User {
    private Integer id;
    private String name;
}
