package com.iterge.iterge_pre.entity;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.iterge.iterge_pre.exception.BizException;
import lombok.Data;

/**
 * @author liuph
 * @date 2023/9/27 15:34:20
 */
@Data
public class Response<T> {
    /**
     * 成功返回ok, 异常返回错误原因。
     */

    /**
     * 返回的协议版本号
     */
    private int version = 0;

    /**
     * 返回的状态码
     */
    private int status = 0;

    /**
     * 错误信息
     */
    @JsonProperty("err_msg")
    @JsonAlias("errMsg")
    private String errMsg = "";

    /**
     * 错误信息
     */
    @JsonProperty("error_msg")
    @JsonAlias("errorMsg")
    private String errorMsg = "";

    /**
     * 服务器时间戳
     */
    private long ts;

    /**
     * 返回的数据
     */
    private T data;

    public Response() {
        ts = System.currentTimeMillis();
    }

    public static <T> Response<T> ok() {
        return ok(null);
    }

    public static <T> Response<T> ok(T data) {
        return ok(0, data);
    }

    public static <T> Response<T> ok(int version, T data) {
        return ok(version, System.currentTimeMillis(), data);
    }

    public static <T> Response<T> ok(int version, long ts, T data) {
        return ok(0, version, ts, "ok", data);
    }

    public static <T> Response<T> ok(int status, int version, long ts, String msg, T data) {
        Response<T> resp = new Response<>();
        resp.setData(data);
        if (null != msg)
            resp.setErrMsg(msg);
        resp.setVersion(version);
        resp.setTs(ts);
        return resp;
    }

    public static <T> Response<T> of(BizException e) {
        Response<T> response = new Response<>();
        response.setStatus(e.getCode());
        response.setErrMsg(e.getMsg());
        response.setErrorMsg(e.getMessage());
        response.setData(null);
        return response;
    }
}
