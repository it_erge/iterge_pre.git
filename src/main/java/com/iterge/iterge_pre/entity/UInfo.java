package com.iterge.iterge_pre.entity;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.iterge.iterge_pre.config.IntToStringSerializer;
import lombok.Data;

/**
 * @author liuph
 * @date 2023/9/22 11:07:12
 */
@Data
//@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class UInfo {
    //@JsonProperty("first_name")
    private String firstName;
    private String lastName;
    @JsonSerialize(using = IntToStringSerializer.class)
    private Integer age;
}
