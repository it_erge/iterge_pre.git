package com.iterge.iterge_pre.exception;

/**
 * @author liuph
 * @date 2023/9/27 15:43:37
 */
public class ErrorCode {
    public static final ErrorCode OK = define(0, "ok");
    public static final ErrorCode PARAM_INVALID = define(-1, "参数错误");
    public static final ErrorCode DB_EXCEPTION = define(-2, "数据库错误");
    public static final ErrorCode REDIS_EXCEPTION = define(-3, "Redis错误");
    public static final ErrorCode MEMCACHE_EXCEPTION = define(-4, "Memcache错误");
    public static final ErrorCode MQ_EXCEPTION = define(-5, "MQ错误");
    public static final ErrorCode RPC_EXCEPTION = define(-6, "RPC调用错误");
    public static final ErrorCode CONFIG_EXCEPTION = define(-7, "配置错误");
    public static final ErrorCode DATA_NOT_EXIST = define(-20, "数据不存在");
    public static final ErrorCode AUTHENTICATION_FAILED = define(-41, "认证失败");
    public static final ErrorCode PERMISSION_DENIED = define(-42, "权限不足");
    public static final ErrorCode BUSINESS_EXCEPTION = define(-50, "业务异常");
    public static final ErrorCode UNKNOWN_EXCEPTION = define(-100, "未知异常");
    private int code;
    private String msg;

    protected ErrorCode() {
        this.code = 0;
        this.msg = null;
    }

    private ErrorCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static ErrorCode define(int code, String msg) {
        return new ErrorCode(code, msg);
    }

    public static ErrorCode define(int code) {
        return define(code, (String)null);
    }

    public int getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o != null && this.getClass() == o.getClass()) {
            ErrorCode errorCode = (ErrorCode)o;
            return this.code == errorCode.code;
        } else {
            return false;
        }
    }
}
