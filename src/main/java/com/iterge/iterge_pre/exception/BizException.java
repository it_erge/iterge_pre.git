package com.iterge.iterge_pre.exception;

import lombok.Getter;

/**
 * @author liuph
 * @date 2023/9/27 15:41:56
 */
public class BizException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private ErrorCode errorCode;
    private int code;
    private String msg;

    public BizException(ErrorCode errorCode) {
        this.errorCode = errorCode;
        this.code = errorCode.getCode();
        this.msg = errorCode.getMsg();

    }
    public BizException(int errorCode,String msg) {
        this.code = errorCode;
        this.msg = msg;

    }
    public BizException(ErrorCode errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.code = errorCode.getCode();
        this.msg = errorMessage;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    //重写以提高性能
    @SuppressWarnings("all")
    @Override
    public Throwable fillInStackTrace() {
        return this;
    }

    @Override
    public String toString() {
        return "BizException{" +
                "errorCode=" + errorCode +
                ", code=" + code +
                ", msg='" + msg + '\'' +
                '}';
    }
}
