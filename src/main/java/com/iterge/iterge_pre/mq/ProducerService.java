package com.iterge.iterge_pre.mq;

import com.iterge.iterge_pre.config.RabbitmqConfig;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author liuph
 * @date 2023/10/17 15:52:48
 */
@Component
public class ProducerService {
    @Autowired
    private RabbitMessagingTemplate mqTemplate;

    public void sendMag(String msg){
        mqTemplate.convertAndSend(RabbitmqConfig.EXCHANGE_TOPICS_INFORM, "inform.email", msg);
    }
}
