package com.iterge.iterge_pre.mq;

import com.iterge.iterge_pre.config.RabbitmqConfig;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author liuph
 * @date 2023/10/17 15:53:04
 */
@Component
public class ConsumerService {
    //监听email队列
    @RabbitListener(queues = {RabbitmqConfig.QUEUE_INFORM_EMAIL})
    public void receive_email(Message message, Channel channel){
        String body = new String(message.getBody());
        System.out.println("消费者：QUEUE_INFORM_EMAIL msg_"+body);
    }
    //监听sms队列
    @RabbitListener(queues = {RabbitmqConfig.QUEUE_INFORM_SMS})
    public void receive_sms(Message message, Channel channel){
        String body = new String(message.getBody());
        System.out.println("消费者：QUEUE_INFORM_SMS msg_"+body);
    }

}
