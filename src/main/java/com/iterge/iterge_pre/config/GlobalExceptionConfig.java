package com.iterge.iterge_pre.config;

import com.iterge.iterge_pre.entity.Response;
import com.iterge.iterge_pre.exception.BizException;
import com.iterge.iterge_pre.exception.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * @author liuph
 * @date 2023/9/27 15:37:21
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionConfig {
    @ExceptionHandler(Throwable.class)
    public Response<Void> handleException(Throwable e, HttpServletRequest request) {
        log.error("执行异常，异常信息：接口：{},信息：{}",request.getRequestURI(),e.getMessage());
        return Response.of(new BizException(ErrorCode.UNKNOWN_EXCEPTION, e.getMessage()));
    }

    //自定义异常
    @ExceptionHandler(BizException.class)
    public Response<Void> bizHandleException(BizException e, HttpServletRequest request) {
        log.error("执行异常，异常信息：接口：{},信息：{}",request.getRequestURI(),e.getMsg());
        return Response.of(e);
    }

    //空指针异常
    @ExceptionHandler(NullPointerException.class)
    public Response<Void> nullPointerHandleException(NullPointerException e, HttpServletRequest request) {
        log.error("空指针异常：接口：{},信息：{}",request.getRequestURI(),e.getMessage());
        return Response.of(new BizException(ErrorCode.BUSINESS_EXCEPTION));
    }
}
